from netfilterqueue import NetfilterQueue
from scapy.all import *
import os
import multiprocessing

# SYN flag value used to check if this packet is a SYN packet
SYN = 0x02

INITIAL_SEQ = None

# IP adress of Server 2
SECOND_SERVER = "192.168.1.48"

# Half of the length of the youtube transmission (which is in the trace.csv file).
# This number is to divide the transmission in two (which is the simplest case of sharing the TCP connection).
# Before it is reached, server 1 will send, after, server 2 will send
WHEN_TO_STOP = 6999000

# IP of the client
CLIENT_IP = "192.168.1.171"

s = conf.L3socket(iface="enp0s25")


def callback_in(payload):
    data = payload.get_payload()
    pkt = IP(data)
    pkt[IP].dst = SECOND_SERVER
    # Delete the previous checksums, scapy will compute new ones
    del pkt[IP].chksum
    del pkt[TCP].chksum
    # Send the modified packet to the second server
    s.send(pkt)
    # Tell nfqueue that we want to send the original packet
    payload.accept()


def callback_out(payload):
    global INITIAL_SEQ

    data = payload.get_payload()
    pkt = IP(data)
    if pkt[TCP].flags & SYN:
        # Save the sequence number
        INITIAL_SEQ = pkt[TCP].seq

        # This is a new TCP connection (and not sidechannel related). We should inform the second server until when to drop packets
        sendToSecondServer(INITIAL_SEQ)

    if pkt[TCP].seq < INITIAL_SEQ + WHEN_TO_STOP:
        payload.accept()
    else:
        payload.drop()


def main_out():
    queue_out = NetfilterQueue()

    # Send all tcp packets coming from the server running on this machine to queue 3
    os.system(
        f"iptables -t mangle -A POSTROUTING -p tcp --destination {CLIENT_IP} --sport 1234 -j NFQUEUE --queue-num 3"
    )
    queue_out.bind(3, callback_out)

    try:
        queue_out.run()  # Main loop
    except KeyboardInterrupt:
        print("Stopping outgoing queue handler")
    queue_out.unbind()
    os.system("iptables -F")
    os.system("iptables -X")


def main():
    q = NetfilterQueue()

    # Send all tcp packets coming from the client to queue 2
    os.system(
        f"iptables -t mangle -A PREROUTING -p tcp -s {CLIENT_IP} -j NFQUEUE --queue-num 2"
    )
    q.bind(2, callback_in)

    process_out = multiprocessing.Process(target=main_out)
    process_out.start()

    try:
        q.run()  # Main loop
    except KeyboardInterrupt:
        print("Stopping incoming queue handler")
    q.unbind()
    process_out.terminate()
    os.system("iptables -F")
    os.system("iptables -X")


s2 = conf.L3socket(iface="enp0s25")


def sendToSecondServer(sequenceNumber):
    """Inform the second server from when it should send.
    This will send the sequence number via UDP to the second server
    """

    whenToStart = str(sequenceNumber)
    print(sequenceNumber)
    print("Sending initial sequence number: " + whenToStart)
    s2.send(IP(dst=SECOND_SERVER) / UDP(dport=12345) / Raw(load=whenToStart))


if __name__ == "__main__":
    main()
