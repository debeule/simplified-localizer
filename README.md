This repo contains code for setting up a simple client and server for a replay (`simpleClientServerReplay.py`), a csv file to test this replay with, and `server_1_duplicate_and_change.py` and `server_2_tcp_modification.py` which are to be run on their respective servers.

To use:

* Run `simpleClientServerReplay.py` as server on both servers (with the same trace file)
* Adapt the `CLIENT_IP` and `SECOND_SERVER` constants as needed, then run `server_1_duplicate_and_change.py` and `server_2_tcp_modification.py` on servers 1 and 2 respectively.
* Run `simpleClientServerReplay.py` as client on the client
