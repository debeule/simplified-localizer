"""
This code runs on server 2 and will modify the TCP flow while coordinating this
with server 1.

It runs two processes that each listen to an nfqueue queue, one that intercepts
incoming TCP traffic and one that intercepts outgoing TCP traffic.
"""

from netfilterqueue import NetfilterQueue
from scapy.all import *
import os
import socket
import multiprocessing
import ctypes
from multiprocessing import Value


# IP of the client
CLIENT_IP = "192.168.1.171"

# SYN flag value used to check if this packet is a SYN packet
SYN = 0x02
SYNACK = 0x012

# "Value" is shared memory accross processes. So we can share the initial SEQ values accross the two processes.
# This is the initial SEQ we get from server 1
INITIAL_SEQ = Value(ctypes.c_uint, 0)
# This is the initial SEQ that is sent from this machine
OUR_INITIAL_SEQ = Value(ctypes.c_uint, 0)

LARGEST_EXPECTED_ACK = Value(ctypes.c_uint, 0)

# Half of the length of the youtube transmission (which is in the trace.csv file).
# This number is to divide the transmission in two (which is the simplest case of sharing the TCP connection). Before it is reached, server 1 will send, after, this server (server 2) will send
WHEN_TO_STOP = 6999000

# This is the socket we will listen on for the backchannel between the two servers
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(("192.168.1.48", 12345))

# Called for outgoing packets. We need to modify or drop them depending on how far along we are in the transmission
def callback(payload):
    # So that python doesn't think there's a namespace conflict
    global INITIAL_SEQ, OUR_INITIAL_SEQ

    # Construct scapy packet structure
    data = payload.get_payload()
    pkt = IP(data)

    # Necessary for with real wehe server (among other things).
    # Commented out for the simple server
    #   # Drop any responses that are non-data (i.e. side channel from the server)
    #   # Side channel responses are already sent by server 1
    #   if pkt[TCP].sport != 443:
    #       payload.drop()
    #       return

    # Set initial SEQ on this server (so that we can calculate the SEQ we want
    # to send as compensated by this and server 1's initial SEQ
    if pkt[TCP].flags & 0b1111_1111 == SYNACK:
        print("Got SYNACK")
        pkt.show()
        OUR_INITIAL_SEQ.value = pkt[TCP].seq

        # Wait for the initial sequence number of the other server here
        print("Waiting for initial sequence number")
        dataSeq, _ = sock.recvfrom(1024)
        INITIAL_SEQ.value = int(dataSeq.decode())
        print("Got initial sequence number: " + str(INITIAL_SEQ.value))

        expected_ack = pkt[TCP].seq + 1 
    else:
        expected_ack = pkt[TCP].seq + len(pkt[TCP].payload)

    LARGEST_EXPECTED_ACK.value = expected_ack

    if INITIAL_SEQ.value == 0:
        payload.drop()
        return
    # Drop if sequence number is less than the value from which we want to start
    elif pkt[TCP].seq < OUR_INITIAL_SEQ.value + WHEN_TO_STOP:
        print(
            "INITIAL_SEQ: "
            + str(INITIAL_SEQ.value)
            + ", Current seq: "
            + str(pkt[TCP].seq)
            + ", Next seq: "
            + str(pkt[TCP].seq + len(pkt[TCP].payload))
        )
        payload.drop()
        return

    # We want to pretend this is coming from the same server
    pkt[IP].src = "192.168.1.62"
    pkt[TCP].seq -= OUR_INITIAL_SEQ.value + INITIAL_SEQ.value
    
    # Delete the previous checksum, scapy will compute a new one
    del pkt[IP].chksum
    del pkt[TCP].chksum

    # Tell nfqueue that we want to accept the modified packet
    try:
        built = pkt.build()
    except ValueError:  # there is some bug in how we calculate seq and ack values?
        print(f"Got a ValueError, seq is {pkt[TCP].seq}")
        payload.drop()
        return

    payload.set_payload(built)
    payload.accept()


def callback_in(payload):
    data = payload.get_payload()
    pkt = IP(data)

    # When getting the initial SYN we don't want to mess with it, return
    if pkt[TCP].flags & 0b1111_1111 == SYN:
        print("Got SYN from client!")
        payload.accept()
        return

    # Busy wait for the initial SEQ values to be there
    while INITIAL_SEQ.value == 0:
        continue
    while OUR_INITIAL_SEQ.value == 0:
        continue
    

    # We want our client to understand the acks, so we revert the ack value to
    # what the client will understand:
    pkt[TCP].ack += OUR_INITIAL_SEQ.value - INITIAL_SEQ.value

    # Busy wait until we sent a SEQ that is bigger or equal to this ack
    while LARGEST_EXPECTED_ACK.value < pkt[TCP].ack:
        continue

    print(pkt[TCP].ack, ", largest expected: ", LARGEST_EXPECTED_ACK.value)

    # Delete the previous checksums, scapy will compute new ones
    del pkt[IP].chksum
    del pkt[TCP].chksum

    # Tell nfqueue that we want to accept the modified packet
    try:
        built = pkt.build()
    except ValueError:
        print(f"Got a ValueError, ack is {pkt[TCP].ack}")
        payload.drop()
        return
    payload.set_payload(built)
    payload.accept()


def main_in(our_init_seq_value, init_seq_value, largest_expected_ack_value):
    # Make sure the two processes share the same Values
    OUR_INITIAL_SEQ = our_init_seq_value
    INITIAL_SEQ = init_seq_value
    LARGEST_EXPECTED_ACK = largest_expected_ack_value

    # Send all tcp packets coming from the client to the queue
    os.system(
        f"iptables -t mangle -A PREROUTING -p tcp -s {CLIENT_IP} -j NFQUEUE --queue-num 2"
    )

    q = NetfilterQueue()
    # Number of the queue indicated in the iptables command (default is 0)
    q.bind(2, callback_in)

    try:
        q.run()  # Main loop
    except KeyboardInterrupt:
        print("Stopping ingoing queue handler")
    q.unbind()
    os.system("iptables -F")
    os.system("iptables -X")


def main():
    # Send all tcp packets going to the client to the queue
    os.system(
        f"iptables -t mangle -A POSTROUTING -p tcp -d {CLIENT_IP} -j NFQUEUE --queue-num 1"
    )
    q = NetfilterQueue()
    # Number of the queue indicated in the iptables command (default is 0)
    q.bind(1, callback)

    process_in = multiprocessing.Process(
        target=main_in, args=(OUR_INITIAL_SEQ, INITIAL_SEQ, LARGEST_EXPECTED_ACK)
    )
    process_in.start()

    try:
        q.run()  # Main loop
    except KeyboardInterrupt:
        print("Stopping outgoing queue handler")
    q.unbind()
    process_in.terminate()
    os.system("iptables -F")
    os.system("iptables -X")


if __name__ == "__main__":
    main()
