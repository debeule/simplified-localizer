"""
adapted from code by Zeinab Shmeis (zeinab.shmeis@epfl.ch)

Example:
    python3 replayBackground.py --server --server_ip=0.0.0.0
    python3 replayBackground.py --kill_server
    python3 replayBackground.py --client --trace_file=traces/link_0_trace_5.csv --server_ip=0.0.0.0
"""

import random, socket, shutil, os, sched, time, argparse, subprocess
import pandas as pd
import numpy as np
from multiprocessing import Process

SERVER_PORT = 1234


def run_server(trace_file, server_ip="0.0.0.0"):
    print("Start a TCP server")
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind((server_ip, SERVER_PORT))
    server.listen(10000)

    if not (".csv" in trace_file):
        print("trace must be a .csv file")
        print(trace_file)
        return

    df = pd.read_csv(trace_file, names=["id", "time", "payload_size"], index_col=0)

    time.sleep(df["time"].values[0])
    print("Start a server running trace {}".format(trace_file))

    while True:
        connection, address = server.accept()
        Process(
            target=accept_connection,
            kwargs={"connection": connection, "address": address, "df": df},
        ).start()


def accept_connection(connection, address, df):
    print("Server accepted a connection from {}".format(address))
    send_scheduler = sched.scheduler(time.time, time.sleep)
    for idx, event in df.iterrows():
        send_scheduler.enter(
            event.time,
            1,
            connection.send,
            argument=(np.random.bytes(event.payload_size),),
        )

    send_scheduler.run()
    connection.shutdown(socket.SHUT_RDWR)
    connection.close()
    print(f"Finished sending to {address}")


def kill_server():
    try:
        output = subprocess.check_output(
            "fuser {}/{} 2>/dev/null".format(SERVER_PORT, "tcp"), shell=True
        )
        os.system("sudo kill -9 {}".format(int(output)))
    except Exception as e:
        print("NO RUNNING SERVER")


def run_client(server_ip="0.0.0.0"):
    print("Running client")
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((server_ip, SERVER_PORT))

    while True:
        data = client_socket.recv(4096)
        if not data:
            break

    client_socket.close()


if __name__ == "__main__":

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--server", action="store_true")
    arg_parser.add_argument("--client", action="store_true")
    arg_parser.add_argument("--server_ip", default="0.0.0.0")
    arg_parser.add_argument("--trace_file")
    arg_parser.add_argument("--kill_server", action="store_true")
    args = arg_parser.parse_args()

    if args.server:
        run_server(args.trace_file, args.server_ip)
    elif args.kill_server:
        kill_server()
    elif args.client:
        run_client(args.server_ip)
