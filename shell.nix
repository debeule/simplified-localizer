# shell.nix
{ pkgs ? import <nixpkgs> {} }:
let
  nfqueue_python = ps: ps.callPackage ./nfqueue.nix {};
  python-with-my-packages = pkgs.python39.withPackages (p: with p; [
netaddr
psutil
gevent
setuptools
netifaces
requests
pandas
scapy
numpy
multiprocess
(nfqueue_python p)
  ]);
in
pkgs.mkShell {
  buildInputs = [
    python-with-my-packages
    pkgs.iperf
    pkgs.tcpdump
    pkgs.inetutils
    pkgs.wireshark
    pkgs.tcpreplay
  ];
}
