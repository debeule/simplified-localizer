{ lib
, stdenv
, buildPythonPackage
, fetchPypi
, xcbuild
, darwin,
pkgs
}:

buildPythonPackage rec {
  pname = "NetfilterQueue";
  version = "1.0.0";
buildInputs = with pkgs;[
libnetfilter_queue
libnfnetlink
];

  src = fetchPypi {
    inherit pname version;
    sha256 = "507be475d8c9f98834763aacf2f6cfe800b253ccd283f14f3d6f89a4f87a5878";
  };
}
